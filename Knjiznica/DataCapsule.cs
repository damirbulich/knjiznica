﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjiznica
{
    public class DataCapsule<T> // klasa koja sluzi za dohvat kolekcije podataka s APIja
    {
        public T[] Data { get; set; } // genericka lista objekaat koji se dohvacaju sa APIja
    }
}
