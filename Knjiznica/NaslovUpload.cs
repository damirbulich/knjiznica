﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjiznica
{
    class NaslovUpload : IPostData // klasa u kojoj su definirani podatci za stvaranje novog naslova i slanje na API
    {
        public string Id;
        public string Naslov;
        public int Author;

        public NaslovUpload(string oznaka, string naslov, int author)
        {
            Id = oznaka;
            Naslov = naslov;
            Author = author;
        }

        public string SerializeData() // vraca JSON objekt s podatcima u stringu, spreman za slanje na API
        {
            return $"{{\"id\":\"{Id}\",\"naslov\":\"{Naslov}\",\"author_id\":{Author}}}";
        }
    }
}
