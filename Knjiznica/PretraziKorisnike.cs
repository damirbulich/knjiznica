﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Knjiznica
{
    public delegate void KorisnikClicked(object source);
    public partial class PretraziKorisnike : UserControl
    {
        public event KorisnikClicked KorisnikKliknut; // custom event za odabir korisnika iz tablice
        public KorisnikModel Odabran { get; set; } // varijabla za spremanje odabranog korisnika iz tablice
        public PretraziKorisnike()
        {
            InitializeComponent();
        }

        private async void PretraziKorisnike_VisibleChanged(object sender, EventArgs e) // kada se prikaze usercontrol...
        {
            if (!DesignMode) // sprijecava crashavanje visual studia pri otvaranju Form1.cs(design)
            {
                if (Visible && !Disposing)
                {
                    dataGridView1.DataSource = await KorisnikController.GetKorisnikAsync(); // dohvacamo korisnike sa APIja i punimo tablicu
                }
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e) // kada se klikne na redak u tablici
        {
            if (!DesignMode) // sprijecava crashavanje visual studia pri otvaranju Form1.cs(design)
            {
                if (e.RowIndex != -1)
                {
                    if (dataGridView1.CurrentCell != null && dataGridView1.CurrentCell.Value != null)
                    {
                        Odabran = dataGridView1.CurrentRow.DataBoundItem as KorisnikModel; // spremamo odabranog kosrisnika iz tablice
                        KorisnikKliknut?.Invoke(this); // triggeramo event za odabir korisnika
                    }
                }
            }
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox1.Text.Trim() == "")
                {
                    try
                    {
                        // dohvacamo posudbe sa APIja i punimo tablicu
                        dataGridView1.DataSource = await KorisnikController.GetKorisnikAsync();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else
                {
                    dataGridView1.DataSource = await SearchController.SearchKorisnikAsync(textBox1.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
