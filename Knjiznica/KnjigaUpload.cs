﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjiznica
{
    class KnjigaUpload : IPostData // klasa u kojoj su definirani podatci za stvaranje nove knjige i slanje na API
    {
        public string Title;
        public int Library;

        public KnjigaUpload(string title, int library)
        {
            Title = title;
            Library = library;
        }

        public string SerializeData() // vraca JSON objekt s podatcima u stringu, spreman za slanje na API
        {
            return $"{{\"title_oznaka\":\"{Title}\",\"library_id\":{Library}}}";
        }
    }
}
