﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjiznica
{
    class KnjigaModel // klasa u kojoj je definiran model podataka knjige kojeg dohvacamo sa APIja
    {
        public int Id { get; set; }
        public NaslovModel Title { get; set; }
        public KnjiznicaModel Library { get; set; }

        public override string ToString()
        {
            return $"{Id}";
        }
    }
}
